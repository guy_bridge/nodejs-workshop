// Author: Guy Bridge
// Date: 06/01/2019
// Description: NodeJS workshop API

// import the required dependencies
const express = require("express");
const bodyParser = require("body-parser");
const LISTEN_PORT = 3000;

// instantiate express
const app = express();

// import routes
const searchRoutes = require("./routes/searchRoutes");

app.use(bodyParser.json());

////////////////////
// Define routes ///
////////////////////

// GET route
app.get("/", (req, res, next) =>{
    res.json({result: "Hi from the API"})
});

// A sample login route using a POST
app.post("/login", (req, res, next)=>{
    

    if(req.body.email && req.body.password)
    {
        // TODO: Validate email and password against database
        res.json({result: "Success - logged in OK"});
    }
    else
    {
        // Handle the errors
        return next(new Error("You need to supply a username and password"));
    }

});

const checkAuth = (req, res, next) => {
    if(req.headers.token === "secretToken")
    {
        // Continue
        next();
    }
    else
    {
        let error = new Error("Missing or invalid auth token");
        error.status = 401;
        return next(error);
    }
};

app.get("/settings", checkAuth, (req, res, next)=>{
    res.json({result: "Settings route..."})
})

app.use("/search", searchRoutes);


// Catch 404 route errors
app.use((req, res, next)=>{
    let error = new Error("API endpoint not found");
    error.status = 404;
    next(error);
})

// Global error handler
app.use((error, req, res, next)=>{
    res.status(error.status || 500);
    res.json({error: {message: error.message}});
});

// Setup the listener
app.listen(LISTEN_PORT, ()=>{
    console.log("NodeJS app listening on TCP/"+LISTEN_PORT);
});