// Search routes

const express = require("express");
const router = express.Router();

router.get("/", (req, res, next)=>{
   
    if(req.query.q) return res.json({result: "You searched for " + req.query.q});

    return next(new Error("Missing search query"));
  
})


module.exports = router;